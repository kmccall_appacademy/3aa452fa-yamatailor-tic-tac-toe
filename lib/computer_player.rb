require 'byebug'

class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name = "Comp")
    @name = name
  end

  def display(board)
    @board = board
  end

  def winner?(move)
    row, column = move
    @board.grid[row][column] = mark
    if board.winner == mark
      @board.grid[row][column] = nil
      return true
    else
      @board.grid[row][column] = nil
      return false
    end
  end

  def get_move
    moves = []
    0.upto(2) do |i|
      0.upto(2) do |j|
        moves << [i, j] if board.grid[i][j] == nil
      end
    end

    moves.find { |move| winner?(move) }
  end
end




















#
#
#
# attr_accessor :name, :board
#
# def initialize(name)
#   @name = name
# end
#
# def display
#   @grid
# end
#
# def get_move
#
# end
#
# def mark
#
# end
