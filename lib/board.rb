require 'byebug'

class Board
  attr_accessor :grid, :mark

  def initialize(grid = Array.new(3) { Array.new(3) })
    @grid = grid
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    winning_symbol = nil
    (diagonal + rows + column).each do |row|
      winning_symbol = :X if row == %i(X X X)
      winning_symbol = :O if row == %i(O O O)
    end

    winning_symbol
  end

  def over?
    @grid.flatten.none? { |pos| pos.nil? } || winner
  end

  private

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, mark)
    x, y = pos
    @grid[x][y] = mark
  end

  def diagonal
    _left = @grid.each_with_index.inject([]) do |arr, (row, i)|
      arr << row[i]
    end

    _right = @grid.each_with_index.inject([]) do |arr, (row, i)|
      arr << row[2 - i]
    end
    [_left, _right]
  end

  def column
    new_arr = [[], [], []]

    @grid.each_with_index do |_row, i|
      @grid.each_with_index do |rows, _j|
        new_arr[i] << rows[i]
      end
    end

    new_arr
  end

  def rows
    @grid.map { |row| row }
  end
end




































# attr_accessor :grid, :mark
#
# def initialize(grid = nil)
#   grid == nil ? @grid = Array.new(3) { Array.new(3) } : @grid = grid
# end
#
# def grid
#   @grid
# end
#
# def place_mark(pos, mark)
#   @grid[pos[0]][pos[1]] = mark
# end
#
# def empty?(pos)
#   @grid[pos[0]][pos[1]] == nil ? true : false
# end
#
# def winner
#
# end
#
# def over?
#   @grid.each do |row|
#     return false if row.all? { |el| el == nil }
#     return true if row.all? { |el| el == :X || :O }
#   end
# end
