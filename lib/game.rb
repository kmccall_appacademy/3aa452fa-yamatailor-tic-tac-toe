require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :player_one, :player_two, :board, :mark

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @current_player = player_one
    @board = Board.new
  end

  def board
    @board
  end

  def play_turn
    current_move = @current_player.get_move
    board.place_mark(current_move, @current_player.mark)
    self.switch_players!
  end

  def current_player
    @current_player
  end

  def switch_players!
    if @current_player == player_one
      @current_player = player_two
    else
      @current_player = player_one
    end
  end
end
